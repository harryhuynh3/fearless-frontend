function Nav() {
    return (
        <nav className='navbar navbar-expand-lg navbar-light bg-light'>
            <a className='navbar-brand' href="#">Conference Go!</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className='collapse navbar-collapse' id="navbarSupportedContent">
                <ul className='navbar-nav mr-auto mb-2 mb-lg-0'>
                    <li className='nav-item'>
                        <a className='nav-link active' aria-current='page' href="#">Home</a>
                    </li>
                    <li className='nav-item'>
                        <a className='nav-link' href='new-location.html'>New Location</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">New conference</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" aria-current='page' href="#">New presentation</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
  }
  
  export default Nav;
