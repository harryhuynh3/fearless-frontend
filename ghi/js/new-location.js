window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
    }
            });
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url);
    const data = await response.json();
    if (!response.ok) {
    } else {
        const stateTag = document.getElementById('state');
        for (let state of data.states) {
            const optionDiv = document.createElement('option');
            optionDiv.value = state.state_abbreviation
            optionDiv.innerHTML = state.state_name
            stateTag.appendChild(optionDiv)
        }
    }
}
)
