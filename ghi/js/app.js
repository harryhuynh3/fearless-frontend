window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, description, pictureUrl, starts, ends, subtitle) {
        return `
            <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle">${subtitle}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <p class="card-text">Starts:  ${starts}</p>
                <p class="card-text">Ends:  ${ends}</p>
            </div>
            </div>
        `;
    }

            // <div class="card" aria-hidden="true">
            //   <img src="..." class="card-img-top" alt="...">
            //   <div class="card-body">
            //     <h5 class="card-title placeholder-glow">
            //       <span class="placeholder col-6"></span>
            //     </h5>
            //     <p class="card-text placeholder-glow">
            //       <span class="placeholder col-7"></span>
            //       <span class="placeholder col-4"></span>
            //       <span class="placeholder col-4"></span>
            //       <span class="placeholder col-6"></span>
            //       <span class="placeholder col-8"></span>
            //     </p>
            //     <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
            //   </div>
            // </div>

    function createAPIResponseError(){
        return `
            <div class="alert alert-primary" role="alert">
                Unsuccessful API response.
            </div>
        `
    }
    function createErrorMessage(){
        return `
            <div class="alert alert-danger" role="alert">
                A simple danger alert—check it out!
            </div>
        `
    }
  const url = 'http://localhost:8000/api/conferences/';
  try {
    const response = await fetch(url);
    if (!response.ok) {
      // Figure out what to do when the response is bad
      createAPIResponseError();
    } else {
      const data = await response.json();
      console.log(data);
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        console.log(conference);
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          let starts = new Date(details.conference.starts);
          starts = starts.toDateString();
          let ends = new Date(details.conference.ends);
          ends = ends.toDateString();
          const subtitle = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, starts, ends, subtitle);
          const columns = document.querySelectorAll('.col');
          for (let column of columns){
            column.innerHTML += html;
          }
        }
      }
    }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
    createErrorMessage();
  }
});
